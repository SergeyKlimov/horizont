$('.small-circle-btn_wrap').on('mouseenter', function() {
    $('.parallax_light').addClass('animate').addClass('orange').addClass('light');
});

$('.small-circle-btn_wrap').on('mouseleave', function() {
    $('.parallax_light').removeClass('animate').removeClass('orange').removeClass('light');
});

$('.parallax_btn-circle').on('mouseenter', function() {
    $('.parallax_light').addClass('animate').addClass('light');
});

$('.parallax_btn-circle').on('mouseleave', function() {
    $('.parallax_light').removeClass('animate').removeClass('light');
});

function parallaxItem() {
  // if ($('body').height() - $('.bottom-side-bg-wrapper').height() - $(window).height() <= $(window).scrollTop()) {
    
  // }
  if ($('.bottom-side-bg-wrapper').get(0).getBoundingClientRect().top - $(window).height() <= 0 ) {
      $('.bottom-side-bg-wrapper').css('margin-top', $('.top-side-bg-wrapper').height() + 'px');
      $('.top-side-bg-wrapper').css({'position': 'fixed', 'bottom': 0, 'padding-right': '17px'});
  } else if ($('.bottom-side-bg-wrapper').get(0).getBoundingClientRect().top - $(window).height() > 10) {
      $('.top-side-bg-wrapper').css({'position': 'relative', 'bottom': 0})
      $('.bottom-side-bg-wrapper').css('margin-top', 0)
  }
}

function getErrorScatter() {
    $('.scatter-notification.error').addClass('active')
    $('.scatter-notification.info').addClass('disable')
    $('.scatter-notification.info-proxy').addClass('disable')
    $('.loader-set-proxy, .loader-vote').removeClass('active')
    setTimeout(function() {
        $('.scatter-notification.error').removeClass('active');
        $('.scatter-notification.info').removeClass('disable')
        $('.scatter-notification.info-proxy').removeClass('disable')
    }, 5000)
  }
function getInfoScatter() {
    $('.loader-set-proxy, .loader-vote').removeClass('active')
  }
  $('#set-proxy').click(function() {
    $('.loader-set-proxy').addClass('active')
  })
  $('#vote-producer').click(function() {
    $('.loader-vote').addClass('active')
  })

  
$('#vote-producer, #set-proxy').on('mouseleave', function () {
    var hideModalInfo = setTimeout(function() {$('.scatter-notification').removeClass('active');}, 3000)
    $('#vote-producer').on('mouseenter', function () {
        clearTimeout(hideModalInfo)
    });
    $('#set-proxy').on('mouseenter', function () {
        clearTimeout(hideModalInfo)
    });
});
$('#vote-producer').on('mouseenter', function () {
    
     $('.scatter-notification').removeClass('active');
    $('.scatter-notification.info').addClass('active');
    // if ($('.scatter-notification.info').hasClass('active')) {
    
    // }
});
$('#set-proxy').on('mouseenter', function () {
 
     $('.scatter-notification').removeClass('active');
    $('.scatter-notification.info-proxy').addClass('active');
    // if ($('.scatter-notification.info-proxy').hasClass('active')) {
        
    // }
});

$('.cross-notification').click(function() {
    $(this).parent().removeClass('active')
});

// $('#set-proxy, #vote-producer').on('mouseleave', function () {
// 	$('.scatter-notification.info').removeClass('active');
// });
