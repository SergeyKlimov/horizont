$('.open-nav').click(function() {
    $('.header').toggleClass('active')
    $('.burger').toggleClass('active')
})

$('.anchor').on('click', function (e) {
	var id = $(this).attr('href')
	$('html, body').stop().animate({
		scrollTop: $(id).offset().top - 150
	}, 400);
	e.preventDefault();
});

$('.open-sub-link').on('mouseenter', function () {
	$('.sub-link_container').addClass('active');
});

$('.open-sub-link').on('mouseleave', function () {
	$('.sub-link_container').removeClass('active');
});