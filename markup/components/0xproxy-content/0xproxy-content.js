$(function() {
    if ($('#slide-page').length > 0 && $(window).width() >= 1024) {
        var scroll_check = 0;
        var scroll_page = $('#slide-page').val();
        $('.proxyx0-page').bind('mousewheel DOMMouseScroll MozMousePixelScroll', function(event) {
            delta = parseInt(event.originalEvent.wheelDelta || -event.originalEvent.detail);
            //больше 1024 по ширине и 650 по высоте есть слайдовая прокрутка
            //если второй слайд без прокрутки
            if (!$('.proxy_block-2_wrap').hasClass('animate')) {
                if (scroll_check == 0) {
                    scroll_check = 1;
                    //скролл вниз
                    if (delta >= 0) {
                        proxyScrollTop();
                    } else {
                        //скролл вверх
                        proxyScrollBottom();
                    }
                }
            } else if ($('.proxy_block-2_wrap').hasClass('animate') && $('.proxy_block-2_container').get(0).getBoundingClientRect().top == 0) {
                //если при скроле 2 слайд до упора вверху убираем прокрутку и он может перелистываться
                if (scroll_check == 0 && delta >= 0) {
                    $('.proxy_block-2_wrap').removeClass('animate');
                    scroll_check = 1;
                    proxyScrollTop();
                }
            }
        });

        // анимация скрола вниз
        function proxyScrollTop() {
            if (scroll_page == 'proxy') {
                //анимация кнопки
                $('.proxy_main-btn').removeClass("animate");

                setTimeout(function() {
                    //анимация нижнего блока с текстом
                    $($('.proxy_single-text').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 50));
                    });

                }, 100);

                setTimeout(function() {
                    //анимация правого блока с текстом
                    $($('.proxy_double-text_right').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 50));
                    });

                }, 200);

                setTimeout(function() {
                    //анимация левого блока с текстом
                    $($('.proxy_double-text_left').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 50));
                    });

                }, 400);

                setTimeout(function() {
                    //анимация заголовка
                    $('.proxy_title-text').removeClass('animate');

                }, 650);

                setTimeout(function() {
                    //анимация планеты
                    $('.proxy_block-2_planet').removeClass('animate');

                }, 700);

                setTimeout(function() {
                    //анимация появления первого слайда
                    $('.proxy_block-1_wrap').removeClass('animate');
                    scroll_check = 0;
                }, 1200);

            } else if (scroll_page == 'bp') {

                setTimeout(function() {
                    //анимация планет
                    $('.bp_planet-wrap ').removeClass('animate');

                }, 0);

                // setTimeout(function() {
                //     //анимация второго блока с текстом
                //     $($('.bp_text-3').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                //         setTimeout(function() {
                //             $(el).removeClass("animate");
                //         }, 0 + (i * 80));
                //     });
                // }, 150);

                setTimeout(function() {
                    //анимация заголовка
                    $('.bp_title-2').removeClass('animate');

                }, 350);

                setTimeout(function() {
                    //анимация кнопки
                    $('.bp_block-2_right').removeClass('animate');

                }, 500);

                setTimeout(function() {
                    //анимация четвертого блока с текстом
                    $($('.bp_text-4').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 600);

                setTimeout(function() {
                    //анимация третьего блока с текстом
                    $($('.bp_text-3').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 700);

                setTimeout(function() {
                    //анимация второго блока с текстом
                    $($('.bp_text-2').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 800);

                setTimeout(function() {
                    //анимация первого блока с текстом
                    $($('.bp_text-1').find('.proxy_line-wrap').get().reverse()).each(function(i, el) {
                        setTimeout(function() {
                            $(el).removeClass("animate");
                        }, 0 + (i * 120));
                    });

                }, 950);

                setTimeout(function() {

                $('.proxy_block-2_container').removeClass('animate');

                }, 1050);

                setTimeout(function() {
                    //анимация заголовка
                    $('.bp_title-1').removeClass('animate');

                }, 1150);

                setTimeout(function() {
                    //анимация появления первого слайда
                    $('.proxy_block-1_wrap').removeClass('animate');
                    scroll_check = 0;
                }, 1250);

            }

        }

        // анимация скрола вверх
        function proxyScrollBottom() {
            if (scroll_page == 'proxy') {
                //анимация исчезновния первого слайда
                $('.proxy_block-1_wrap').addClass('animate');

                setTimeout(function() {
                    //анимация планеты
                    $('.proxy_block-2_planet').addClass('animate');

                }, 100);

                setTimeout(function() {
                    //анимация заголовка
                    $('.proxy_title-text').addClass('animate');

                }, 300);

                setTimeout(function() {
                    //анимация левого блока с текстом
                    $('.proxy_double-text_left').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 80));
                    });

                }, 400);

                setTimeout(function() {
                    //анимация правого блока с текстом
                    $('.proxy_double-text_right').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 650);

                setTimeout(function() {
                    //анимация нижнего блока с текстом
                    $('.proxy_single-text').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 150));
                    });
                }, 970);

                setTimeout(function() {
                    //анимация кнопки
                    $('.proxy_main-btn').addClass("animate");
                }, 1170);

                setTimeout(function() {
                    //включает прокрутку второго слайда
                    $('.proxy_block-2_wrap').addClass('animate');
                    scroll_check = 0;
                }, 1200);
            } else if (scroll_page == 'bp') {
                //анимация первого слайда
                $('.proxy_block-1_wrap').addClass('animate');

                setTimeout(function() {
                    //анимация фона из точек
                    $('.proxy_block-2_container').addClass('animate');

                }, 100);

                setTimeout(function() {
                    //анимация заголовка
                    $('.bp_title-1').addClass('animate');

                }, 200);

                setTimeout(function() {
                    //анимация первого блока с текстом
                    $('.bp_text-1').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 80));
                    });

                }, 400);

                setTimeout(function() {
                    //анимация второго блока с текстом
                    $('.bp_text-2').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 650);

                setTimeout(function() {
                    //анимация третьего блока с текстом
                    $('.bp_text-3').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 900);

                setTimeout(function() {
                    //анимация четвертого блока с текстом
                    $('.bp_text-4').find('.proxy_line-wrap').each(function(i, el) {
                        setTimeout(function() {
                            $(el).addClass("animate");
                        }, 0 + (i * 80));
                    });
                }, 1150);

                setTimeout(function() {
                    //анимация кнопки
                    $('.bp_block-2_right').addClass('animate');

                }, 1250);

                setTimeout(function() {
                    //анимация заголовка
                    $('.bp_title-2').addClass('animate');

                }, 1350);

                // setTimeout(function() {
                //     //анимация второго блока с текстом
                //     $('.bp_text-3').find('.proxy_line-wrap').each(function(i, el) {
                //         setTimeout(function() {
                //             $(el).addClass("animate");
                //         }, 0 + (i * 80));
                //     });
                // }, 1000);

                setTimeout(function() {
                    //анимация планет
                    $('.bp_planet-wrap ').addClass('animate');

                }, 1450);

                setTimeout(function() {
                    //включает прокрутку второго слайда
                    $('.proxy_block-2_wrap').addClass('animate');
                    scroll_check = 0;
                }, 1550);

            }
        }

        $('body').on('click', '.scroll-btn', function() {
            proxyScrollBottom();
        });

    } else {
        $(window).scroll(function() {
            $('.bp_planet').each(function(i, el) {
                var position = $(window).scrollTop() + $(window).height() / 2;
                var $this = $(el);
                var scroll_test = 0;
                if (position - 180 <= $this.offset().top && position + 180 > $this.offset().top && scroll_test == 0) {
                    $this.addClass('active');
                    scroll_test = 1;
                    setTimeout(function () {
                        scroll_test = 0
                    }, 600);
                } else {
                    if (scroll_test == 0) {
                        $this.removeClass('active');
                    }
                }
            });
        });

        $('body').on('click', '.scroll-btn', function() {
            $('html, body').stop().animate({
                scrollTop: $('#why').offset().top - 80
            }, 400);
        });
    }
});
