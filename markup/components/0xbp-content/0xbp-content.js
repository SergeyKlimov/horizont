$('.bp_planet').on('mouseenter', function () {
	$(this).addClass('active');
	$('.bp_circle').addClass('active');
});

$('.bp_planet').on('mouseleave', function () {
	$(this).removeClass('active');
	$('.bp_circle').removeClass('active');

});

$('.open-scheme').on('click', function(e) {
	$('.mobile-scheme').addClass('active');
	$('meta[name=viewport]').attr('content', 'width=1024, initial-scale=1');
	// $('html').addClass('lb-disable-scrolling');
	e.preventDefault();
});


$('.low-link').click(function() {
	$('html, body').animate({
	  scrollTop: ($($.attr(this, 'href')).offset()).top -90
	}, 500);
	return false;
  });

$('.close-scheme').on('click', function(e) {
	$('.mobile-scheme').removeClass('active');
	$('meta[name=viewport]').attr('content', 'width=device-width, initial-scale=1');
	// $('html').removeClass('lb-disable-scrolling');
    $('html, body').stop().animate({
        scrollTop: $('#technology').offset().top
    }, 400);

	e.preventDefault();
});

