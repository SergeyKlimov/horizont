// function scrollBigShip(argument) {
//     var depth, i, layer, layers, len, movement, topDistance, translate3d;
//     topDistance = this.pageYOffset - 700;
//     layers = document.querySelectorAll("[data-type='parallax-big-ship']");
//     for (i = 0, len = layers.length; i < len; i++) {
//         layer = layers[i];
//         depth = layer.getAttribute('data-depth');
//         movement = -(topDistance * depth);
//         translate3d = 'translate3d(' + movement + 'px, 0, 0)';
//         layer.style['-webkit-transform'] = translate3d;
//         layer.style['-moz-transform'] = translate3d;
//         layer.style['-ms-transform'] = translate3d;
//         layer.style['-o-transform'] = translate3d;
//         layer.style.transform = translate3d;
//     }
// }

// function scrollSmallShip(argument) {
//     var depth, i, layer, layers, len, movement, topDistance, translate3d;
//     topDistance = this.pageYOffset - 700;
//     layers = document.querySelectorAll("[data-type='parallax-ship']");
//     for (i = 0, len = layers.length; i < len; i++) {
//         layer = layers[i];
//         depth = layer.getAttribute('data-depth');
//         movement = -(topDistance * depth) * 1.3;
//         movement2 = topDistance * depth * 0.7;
//         translate3d = 'translate3d(' + movement + 'px, ' + movement2 + 'px, 0)';
//         layer.style['-webkit-transform'] = translate3d;
//         layer.style['-moz-transform'] = translate3d;
//         layer.style['-ms-transform'] = translate3d;
//         layer.style['-o-transform'] = translate3d;
//         layer.style.transform = translate3d;
//         if (movement2 >= 420) {
//             $('.big-zerox-fly-light').addClass('animate');
//         } else {
//             $('.big-zerox-fly-light').removeClass('animate'); 
//         }
//     }
// }