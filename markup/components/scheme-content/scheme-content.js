$('.slider-scheme-about').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    swipe: false,

  });

  $('.slider-scheme-about').on('swipe', function(event, slick, direction){
    $('.scheme-about-nav_item').removeClass('active')
    $('.scheme-about-nav_item').each(function() {
        if ($(this).attr('data-scheme-about') == slick['currentSlide']) {
            $(this).addClass('active')
        }
    });
  });
$('.scheme-about-nav_item').click(function() {
    $('.scheme-about-nav_item').removeClass('active')
    $(this).addClass('active')
    if ($(window.width <= 355)) {
        $('.scheme-about-nav').removeClass('move')
        if ($(this).attr('data-scheme-about') == 2) {
            $('.scheme-about-nav').addClass('move')
        }
    }
    $('.slider-scheme-about').slick('slickGoTo', $(this).attr('data-scheme-about'))
})


$('.scheme-nav_item').on('mouseenter', function () {
    navClassScheme = $(this).attr('data-scheme')
    $('.scheme-nav_item').removeClass('active')
    $('.beacon-wrapper').removeClass('active')
    $('.coonecting-square-top').removeClass('active')
    $('.coonecting-square-bottom').removeClass('active')
    $(this).addClass('active')
    if (navClassScheme == 'eos-api') {
        $('.beacon-wrapper').each(function() {
            if ($(this).attr('data-scheme-triple')  == 'seed-plus-api')
            $(this).addClass('active')
         
        })
    }
    else if (navClassScheme == 'node1') {
        $('.coonecting-square-top').addClass('active')
    }
    else if (navClassScheme == 'node2') {
        $('.coonecting-square-bottom').addClass('active')
    }
    else {
        $('.beacon-wrapper').each(function() {

            if ($(this).attr('data-scheme') ==  navClassScheme) {
                $(this).addClass('active')
            }
        })
    }
   
});


$('.scheme-nav_item').on('click', function () {
    navClassScheme = $(this).attr('data-scheme')
    $(this).addClass('active')
    if (navClassScheme == 'eos-api') {
        $('.beacon-wrapper').each(function() {
            if ($(this).attr('data-scheme-triple')  == 'seed-plus-api')
            $(this).addClass('active')
        });
    }
    else if (navClassScheme == 'node1') {
        $('.coonecting-square-top').addClass('active')
    }
    else if (navClassScheme == 'node2') {
        $('.coonecting-square-bottom').addClass('active')
    }
    else {
        $('.beacon-wrapper').each(function() {

            if ($(this).attr('data-scheme') ==  navClassScheme) {
                $(this).addClass('active')
            }
        })
    }
   
});

$('.hover-zone-scheme').on('mouseenter', function () {
    navClassScheme = $(this).attr('data-scheme')
    $('.beacon-wrapper, .scheme-nav_item').removeClass('active')
    $('.coonecting-square-top').removeClass('active')
    $('.coonecting-square-bottom').removeClass('active')
    if (navClassScheme == 'node1' || navClassScheme == 'node2') {
        if (navClassScheme == 'node1') {
            $('.coonecting-square-top').addClass('active')
        }
        else if (navClassScheme == 'node2') {
            $('.coonecting-square-bottom').addClass('active')
        }
    }
    else {
        $('.beacon-wrapper').each(function() {
            if ($(this).attr('data-scheme')  == navClassScheme)
            $(this).addClass('active')
         
        })
    }
  
    $('.scheme-nav_item').each(function() {
        if ($(this).attr('data-scheme')  == navClassScheme)
        $(this).addClass('active')
     
    })
});


$('.scheme-element-nav').click(function() {
    $('#scheme-desctop').addClass('unactive');
    setTimeout(function () {
        $('#scheme-desctop').removeClass('unactive')
    }, 1200)
    $('.beacon-wrapper, .scheme-nav_item').removeClass('active')
    $('.coonecting-square-top').removeClass('active')
    $('.coonecting-square-bottom').removeClass('active')

    navClassScheme = $(this).attr('data-scheme').split(',')
    
    navClassScheme.forEach(function(item, i, arr) {
        $('.scheme-nav_item[data-scheme='+item+']').click();
        console.log($('.scheme-nav_item[data-scheme='+item+']'));
    });
    // navClassScheme = $(this).attr('data-scheme')
    // if (navClassScheme == 'node1' || navClassScheme == 'active') {
    //     if (navClassScheme == 'node1') {
    //         $('.coonecting-square-top').addClass('active')
    //     }
    //     else if (navClassScheme == 'node2') {
    //         $('.coonecting-square-bottom').addClass('active')
    //     }
    // }
    // else {
    //     $('.beacon-wrapper').each(function() {
    //         if ($(this).attr('data-scheme')  == navClassScheme)
    //         $(this).addClass('active')
         
    //     })
    // }
    // $('.scheme-nav_item').each(function() {
    //     if ($(this).attr('data-scheme')  == navClassScheme)
    //     $(this).addClass('active')
     
    // })
});

$('.low-link').click(function(e) {
    var id = $(this).attr('href');
    if ($(window).width() > 1023) {
        $('.proxy_block-2_content').stop().animate({
            scrollTop: $('#why-block').outerHeight() + $('#transparency').outerHeight()
        }, 500); 
    } else {
        console.log($(id))
        $('html, body').stop().animate({
            scrollTop: $('#technology').offset().top
        }, 500); 
    }

    e.preventDefault();
});



 
