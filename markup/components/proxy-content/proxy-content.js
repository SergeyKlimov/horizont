$('.copy-btn').on('click', function (e) {
	var elem = $(this);
	elem.text('Copied');
	copyToClipboard(elem.attr('data-target'));
	setTimeout(function () {
		elem.text('Copy');
	},1500);
	e.preventDefault();
});

function copyToClipboard(element) {
	var $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
}